﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1_and_2
{
    //Task 2:
    class LinearSearch : SearchStrategy
    {
        public override bool Search(double[] array, double key) {
            int arraySize = array.Length;
            for (int i = 0; i < arraySize; i++)
                if (array[i] == key)
                    return true;
            return false;     
        }
    }
}
