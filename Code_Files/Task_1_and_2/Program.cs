﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_1_and_2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task 1:
            double[] array = { 0.4, 5.7, 2.3, 4.7, 1.8, 6.4, 8.9, 9.1, 7.2, 3.2 };
            NumberSequence numberSequence = new NumberSequence(array);

            SequentialSort sequentialSort = new SequentialSort();
            CombSort combSort = new CombSort();
            BubbleSort bubbleSort = new BubbleSort();

            numberSequence.SetSortStrategy(sequentialSort);
            numberSequence.Sort();
            Console.WriteLine(numberSequence.ToString());

            numberSequence.SetSortStrategy(combSort);
            numberSequence.Sort();
            Console.WriteLine(numberSequence.ToString());

            numberSequence.SetSortStrategy(bubbleSort);
            numberSequence.Sort();
            Console.WriteLine(numberSequence.ToString());

            //Task 2:
            LinearSearch linearSearch = new LinearSearch();
            double numberToSearch;
            numberSequence.SetSearchStrategy(linearSearch);
            Console.WriteLine("Which number would you like to search?");
            numberToSearch = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("The number " + numberToSearch + (numberSequence.Search(numberToSearch) ? " is " : " is not ") + "found.");
        }
    }
}
